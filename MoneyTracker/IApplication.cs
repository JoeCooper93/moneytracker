﻿namespace MoneyTracker
{
	public interface IApplication
	{
		void Run();
	}
}