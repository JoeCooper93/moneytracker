﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.DAL
{
	public class DbInitializer : IDbInitializer
	{
		public static void Initialize(MoneyTrackerContext context)
		{
			context.Database.EnsureCreated();

			// Look for any students.
			if (context.Incomes.Any())
			{
				return;   // DB has been seeded
			}


		}
	}
}
