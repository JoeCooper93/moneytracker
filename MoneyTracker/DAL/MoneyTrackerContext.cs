﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneyTracker.Model;
using Microsoft.EntityFrameworkCore;

namespace MoneyTracker.DAL
{
	public class MoneyTrackerContext : DbContext, IMoneyTrackerContext
	{

		public MoneyTrackerContext(DbContextOptions<MoneyTrackerContext> options) : base(options)
		{
		}

		public DbSet<DebtModel> Debts { get; set; }
		public DbSet<ExpenditureModel> Expenditures { get; set; }
		public DbSet<GoalsModel> Goals { get; set; }
		public DbSet<IncomeModel> Incomes { get; set; }
		public DbSet<SavingsModel> Savings { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<DebtModel>().ToTable("Debts");
			modelBuilder.Entity<ExpenditureModel>().ToTable("Expenditures");
			modelBuilder.Entity<GoalsModel>().ToTable("Goals");
			modelBuilder.Entity<IncomeModel>().ToTable("Incomes");
			modelBuilder.Entity<SavingsModel>().ToTable("Savings");
		}
	}
}
