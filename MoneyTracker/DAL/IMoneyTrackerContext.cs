﻿using Microsoft.EntityFrameworkCore;
using MoneyTracker.Model;

namespace MoneyTracker.DAL
{
	public interface IMoneyTrackerContext 
	{
		DbSet<DebtModel> Debts { get; set; }
		DbSet<ExpenditureModel> Expenditures { get; set; }
		DbSet<GoalsModel> Goals { get; set; }
		DbSet<IncomeModel> Incomes { get; set; }
		DbSet<SavingsModel> Savings { get; set; }


	}
}