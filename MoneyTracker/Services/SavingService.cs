﻿using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using Newtonsoft.Json;

using Microsoft.Extensions.DependencyInjection;

namespace MoneyTracker.Services
{
	public class SavingService : ISavingService
	{
		private static List<SavingsModel> savings = new List<SavingsModel>();


		public SavingService()
		{

		}
		public void LoadData(IMoneyTrackerContext context)
		{
			savings = context.Savings.ToList();

		}
		public IEnumerable<ISavingsModel> GetAll()
		{

			return savings;
		}

		public ISavingsModel GetById(int id)
		{
			return savings.Where(saving => saving.Id == id)
				.FirstOrDefault();
		}

		public ISavingsModel Create(SavingsModel saving)
		{

			savings.Add(saving);

			return saving;
		}

		public void Update(int id, ISavingsModel saving)
		{
			ISavingsModel found = savings.Where(n => n.Id == id)
				.FirstOrDefault();

			found.SavingsAmount = saving.SavingsAmount;
			found.SavingsName = saving.SavingsName;


		}

		public void Delete(int id)
		{
			savings.RemoveAll(n => n.Id == id);
		}
	}
}
