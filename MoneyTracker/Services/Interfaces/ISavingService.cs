﻿using MoneyTracker.DAL;
using MoneyTracker.Model;
using System.Collections.Generic;

namespace MoneyTracker.Services
{
	public interface ISavingService
	{
		ISavingsModel Create(SavingsModel saving);
		void Delete(int id);
		IEnumerable<ISavingsModel> GetAll();
		ISavingsModel GetById(int id);
		void LoadData(IMoneyTrackerContext context);
		void Update(int id, ISavingsModel saving);
	}
}