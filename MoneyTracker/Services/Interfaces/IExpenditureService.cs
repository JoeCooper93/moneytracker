﻿using MoneyTracker.DAL;
using MoneyTracker.Model;
using System.Collections.Generic;

namespace MoneyTracker.Services
{
	public interface IExpenditureService
	{
		IExpenditureModel Create(ExpenditureModel expenditure);
		void Delete(int id);
		IEnumerable<IExpenditureModel> GetAll();
		IExpenditureModel GetById(int id);
		void LoadData(IMoneyTrackerContext context);
		void Update(int id, IExpenditureModel expenditure);

	}
}