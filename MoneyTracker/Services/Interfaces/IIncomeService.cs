﻿using MoneyTracker.DAL;
using MoneyTracker.Model;
using System.Collections.Generic;

namespace MoneyTracker.Services
{
	public interface IIncomeService
	{
		IIncomeModel Create(IncomeModel saving);
		void Delete(int id);
		IEnumerable<IIncomeModel> GetAll();
		IIncomeModel GetById(int id);
		void LoadData(IMoneyTrackerContext context);
		void Update(int id, IIncomeModel income);
	}
}