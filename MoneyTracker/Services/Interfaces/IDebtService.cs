﻿using MoneyTracker.DAL;
using MoneyTracker.Model;
using System.Collections.Generic;

namespace MoneyTracker.Services
{
	public interface IDebtService
	{
		IDebtModel Create(DebtModel debt);
		void Delete(int id);
		IEnumerable<IDebtModel> GetAll();
		IDebtModel GetById(int id);
		void LoadData(IMoneyTrackerContext context);
		void Update(int id, IDebtModel debt);
	}
}