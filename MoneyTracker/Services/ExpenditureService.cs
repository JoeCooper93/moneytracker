﻿using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using Newtonsoft.Json;

using Microsoft.Extensions.DependencyInjection;

namespace MoneyTracker.Services
{


	public class ExpenditureService : IExpenditureService
	{
		private static List<ExpenditureModel> expenditures = new List<ExpenditureModel>();
	

		public ExpenditureService()
		{
			
		}
		public void LoadData(IMoneyTrackerContext context)
		{
			expenditures = context.Expenditures.ToList();
			
		}
		public IEnumerable<IExpenditureModel> GetAll()
		{
			
			return expenditures ;
		}

		public IExpenditureModel GetById(int id)
		{
			return expenditures.Where(expenditure => expenditure.Id == id)
				.FirstOrDefault();
		}

		public IExpenditureModel Create(ExpenditureModel expenditure)
		{
			
			expenditures.Add(expenditure);

			return expenditure;
		}

		public void Update(int id, IExpenditureModel expenditure)
		{
			IExpenditureModel found = expenditures.Where(n => n.Id == id)
				.FirstOrDefault();

			found.ExpenditureType = expenditure.ExpenditureType;
			found.ExpenditurePaymentType = expenditure.ExpenditurePaymentType;
			found.ExpenditureAmount = expenditure.ExpenditureAmount;

		}

		public void Delete(int id)
		{
			expenditures.RemoveAll(n => n.Id == id);
		}
	}
}

