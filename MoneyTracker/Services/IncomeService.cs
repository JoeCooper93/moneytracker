﻿using MoneyTracker.DAL;
using MoneyTracker.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Services
{
	public class IncomeService : IIncomeService
	{
		private static List<IncomeModel> incomes = new List<IncomeModel>();


		public IncomeService()
		{

		}
		public void LoadData(IMoneyTrackerContext context)
		{
			incomes = context.Incomes.ToList();

		}
		public IEnumerable<IIncomeModel> GetAll()
		{

			return incomes;
		}

		public IIncomeModel GetById(int id)
		{
			return incomes.Where(saving => saving.Id == id)
				.FirstOrDefault();
		}

		public IIncomeModel Create(IncomeModel saving)
		{

			incomes.Add(saving);

			return saving;
		}

		public void Update(int id, IIncomeModel income)
		{
			IIncomeModel found = incomes.Where(n => n.Id == id)
				.FirstOrDefault();

			found.IncomeAmount = income.IncomeAmount;
			found.IncomeType = income.IncomeType;


		}

		public void Delete(int id)
		{
			incomes.RemoveAll(n => n.Id == id);
		}
	}
}
