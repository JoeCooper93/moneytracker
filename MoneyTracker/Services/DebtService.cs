﻿using MoneyTracker.DAL;
using MoneyTracker.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Services
{
	public class DebtService : IDebtService
	{
		private static List<DebtModel> debts = new List<DebtModel>();


		public DebtService()
		{

		}
		public void LoadData(IMoneyTrackerContext context)
		{
			debts = context.Debts.ToList();

		}
		public IEnumerable<IDebtModel> GetAll()
		{

			return debts;
		}

		public IDebtModel GetById(int id)
		{
			return debts.Where(debt => debt.Id == id)
				.FirstOrDefault();
		}

		public IDebtModel Create(DebtModel debt)
		{

			debts.Add(debt);

			return debt;
		}

		public void Update(int id, IDebtModel debt)
		{
			IDebtModel found = debts.Where(n => n.Id == id)
				.FirstOrDefault();

			found.DebtAmount = debt.DebtAmount;
			found.DebtIntrest = debt.DebtIntrest;
			found.DebtName = debt.DebtName;


		}

		public void Delete(int id)
		{
			debts.RemoveAll(n => n.Id == id);
		}
	}
}

