using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using MoneyTracker.DAL;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Hosting;

namespace MoneyTracker
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var container = ContainerConfig.Configure();
			using(var scope = container.BeginLifetimeScope())
			{
				var app = scope.Resolve<IApplication>();
				app.Run();
			}
			var host = CreateWebHostBuilder(args).Build();

			CreateDbIfNotExists(host);

			host.Run();
			CreateWebHostBuilder(args).Build().Run();

		}
		private static void CreateDbIfNotExists(IWebHost host)
		{
			using (var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
				try
				{
					var context = services.GetRequiredService<MoneyTrackerContext>();
					DbInitializer.Initialize(context);
				}
				catch (Exception ex)
				{
					var logger = services.GetRequiredService<ILogger<Program>>();
					logger.LogError(ex, "An error occurred creating the DB.");
				}
			}
		}
		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>();
	}
}

