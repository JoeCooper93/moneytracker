using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MoneyTracker.Services;
using MoneyTracker.DAL;

namespace MoneyTracker
{
	public class Startup
	{
		readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<IExpenditureService, ExpenditureService>();
			services.AddSingleton<IExpenditureService, ExpenditureService>();
			services.AddScoped<IExpenditureService, ExpenditureService>();
			services.AddTransient<ISavingService, SavingService>();
			services.AddSingleton<ISavingService, SavingService>();
			services.AddScoped<ISavingService, SavingService>();
			services.AddTransient<IDebtService, DebtService>();
			services.AddSingleton<IDebtService, DebtService>();
			services.AddScoped<IDebtService, DebtService>();
			services.AddTransient<IIncomeService, IncomeService>();
			services.AddSingleton<IIncomeService, IncomeService>();
			services.AddScoped<IIncomeService, IncomeService>();
			services.AddTransient<IMoneyTrackerContext, MoneyTrackerContext>();
			services.AddSingleton<IMoneyTrackerContext, MoneyTrackerContext>();
			services.AddScoped<IMoneyTrackerContext, MoneyTrackerContext>();
			services.AddDbContext<MoneyTrackerContext>(options =>
			  options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
			services.AddDatabaseDeveloperPageExceptionFilter();
			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder
						.AllowAnyMethod()
						.AllowCredentials()
						.SetIsOriginAllowed((host) => true)
						.AllowAnyHeader());
			});
			services.AddControllersWithViews();
		
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseCors("CorsPolicy");
			app.UseRouting();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "api",
					pattern: "{controller}/{action=Index}/{id?}");
			});

			
		}
	}
}
