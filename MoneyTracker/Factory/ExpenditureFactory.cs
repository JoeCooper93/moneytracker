﻿using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using MoneyTracker;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using MoneyTracker.Services;

namespace MoneyTrackerClassLibrary
{
	public  class ExpenditureFactory
	{
        public static T Create<T>()
        {
            if (typeof(T) == typeof(IExpenditureModel))
            {
                return (T)(IExpenditureModel)new ExpenditureModel();
            }
            else
            {
                throw new NotImplementedException(String.Format("Creation of {0} interface is not supported yet.", typeof(T)));
            }
        }
    }
	
}
