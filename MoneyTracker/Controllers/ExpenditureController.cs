﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using MoneyTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace MoneyTracker.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ExpenditureController : ControllerBase
	{
            private readonly IExpenditureService expenditureService;
        private readonly IMoneyTrackerContext _context;

        public ExpenditureController(IExpenditureService expenditure , IMoneyTrackerContext context)
            {
            _context = context;
            this.expenditureService = expenditure;
            expenditureService.LoadData(_context);
            }

            // GET api/users
            [HttpGet]
            public IEnumerable<IExpenditureModel> Get()
            {         
                return expenditureService.GetAll();
            }

            // GET api/users/5
            [HttpGet("{id}")]
            public async Task<IActionResult> Get(int id)
            {
                return Ok(expenditureService.GetById(id));
            }

            // POST api/users
            [HttpPost]
            public async Task<IActionResult> Post([FromBody] ExpenditureModel expenditure)
            {
                return CreatedAtAction("Get", new { id = expenditure.Id }, expenditureService.Create(expenditure));
            }

            // PUT api/users/5
            [HttpPut("{id}")]
            public async Task<IActionResult> Put(int id, [FromBody] IExpenditureModel expenditure)
            {
            expenditureService.Update(id, expenditure);

                return NoContent();
            }

            // DELETE api/users/5
            [HttpDelete("{id}")]
            public async Task<IActionResult> Delete(int id)
            {
            expenditureService.Delete(id);

                return NoContent();
            }

            public override NoContentResult NoContent()
            {
                return base.NoContent();
            }
        }
    }

