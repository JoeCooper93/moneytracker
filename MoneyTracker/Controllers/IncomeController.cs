﻿using Microsoft.AspNetCore.Mvc;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using MoneyTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class IncomeController : Controller
	{

            private readonly IIncomeService incomeService;
            private readonly IMoneyTrackerContext _context;

            public IncomeController(IIncomeService income, IMoneyTrackerContext context)
            {
                _context = context;
                this.incomeService = income;
                incomeService.LoadData(_context);
            }

            // GET api/users
            [HttpGet]
            public IEnumerable<IIncomeModel> Get()
            {
                return incomeService.GetAll();
            }

            // GET api/users/5
            [HttpGet("{id}")]
            public async Task<IActionResult> Get(int id)
            {
                return Ok(incomeService.GetById(id));
            }

            // POST api/users
            [HttpPost]
            public async Task<IActionResult> Post([FromBody] IncomeModel Income)
            {
                return CreatedAtAction("Get", new { id = Income.Id }, incomeService.Create(Income));
            }

            // PUT api/users/5
            [HttpPut("{id}")]
            public async Task<IActionResult> Put(int id, [FromBody] IIncomeModel Income)
            {
                incomeService.Update(id, Income);

                return NoContent();
            }

            // DELETE api/users/5
            [HttpDelete("{id}")]
            public async Task<IActionResult> Delete(int id)
            {
                incomeService.Delete(id);

                return NoContent();
            }

            public override NoContentResult NoContent()
            {
                return base.NoContent();
            }
        }
    }
