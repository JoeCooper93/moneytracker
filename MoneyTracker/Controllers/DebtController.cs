﻿using Microsoft.AspNetCore.Mvc;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using MoneyTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DebtController : Controller
    {

        private readonly IDebtService debtService;
        private readonly IMoneyTrackerContext _context;

        public DebtController(IDebtService debt, IMoneyTrackerContext context)
        {
            _context = context;
            this.debtService = debt;
            debtService.LoadData(_context);
        }

        // GET api/users
        [HttpGet]
        public IEnumerable<IDebtModel> Get()
        {
            return debtService.GetAll();
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(debtService.GetById(id));
        }

        // POST api/users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DebtModel debt)
        {
            return CreatedAtAction("Get", new { id = debt.Id }, debtService.Create(debt));
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] IDebtModel debt)
        {
            debtService.Update(id, debt);

            return NoContent();
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            debtService.Delete(id);

            return NoContent();
        }

        public override NoContentResult NoContent()
        {
            return base.NoContent();
        }
    }
}

