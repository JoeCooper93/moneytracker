﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoneyTracker.DAL;
using MoneyTracker.Model;
using MoneyTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SavingController : ControllerBase
    {
        private readonly ISavingService SavingService;
        private readonly IMoneyTrackerContext _context;

        public SavingController(ISavingService saving, IMoneyTrackerContext context)
        {
            _context = context;
            this.SavingService = saving;
            SavingService.LoadData(_context);
        }

        // GET api/users
        [HttpGet]
        public IEnumerable<ISavingsModel> Get()
        {
            return SavingService.GetAll();
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(SavingService.GetById(id));
        }

        // POST api/users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SavingsModel Saving)
        {
            return CreatedAtAction("Get", new { id = Saving.Id }, SavingService.Create(Saving));
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ISavingsModel Saving)
        {
            SavingService.Update(id, Saving);

            return NoContent();
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            SavingService.Delete(id);

            return NoContent();
        }

        public override NoContentResult NoContent()
        {
            return base.NoContent();
        }
    }
}

