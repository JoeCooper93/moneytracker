﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MoneyTracker.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class SmetherForecastController : ControllerBase
	{
		private static readonly string[] Summaries = new[]
	{
			"Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Smeltering", "Scorching"
		};

		private readonly ILogger<SmetherForecastController> _logger;

		public SmetherForecastController(ILogger<SmetherForecastController> logger)
		{
			_logger = logger;
		}

		[HttpGet]
		public IEnumerable<SmetherForecast> Get()
		{
			var rng = new Random();
			return Enumerable.Range(1, 5).Select(index => new SmetherForecast
			{
				Date = DateTime.Now.AddDays(index),
				TemperatureC = rng.Next(-20, 55),
				Summary = Summaries[rng.Next(Summaries.Length)]
			})
			.ToArray();
		}
	}
}
