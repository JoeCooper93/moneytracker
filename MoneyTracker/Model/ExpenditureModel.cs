﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public class ExpenditureModel : IExpenditureModel
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }
		public string ExpenditureName { get; set; }
		public string ExpenditureType { get; set; }
		public int ExpenditureAmount { get; set; }
		public string ExpenditurePaymentType { get; set; }
	}
}
