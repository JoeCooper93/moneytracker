﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public class DebtModel : IDebtModel
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }
		public string DebtName { get; set; }
		public int DebtIntrest { get; set; }
		public  int DebtAmount { get; set; }

	}
}
