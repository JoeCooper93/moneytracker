﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public class AccountsModel : IAccountsModel
	{
		public string AccountType { get; set; }
		public string AccountName { get; set; }
		public int AccountAmount { get; set; }

		public void PrintStuff()
		{
			Console.WriteLine(AccountAmount);
		}
		
	}
}
