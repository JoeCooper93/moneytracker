﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public class SavingsModel : ISavingsModel
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }
		public string SavingsName { get; set; }
		public int SavingsAmount { get; set; }
	}
}
