﻿namespace MoneyTracker.Model
{
	public interface IExpenditureModel
	{
		int Id { get; set; }
		string ExpenditureName { get; set; }
		int ExpenditureAmount { get; set; }
		string ExpenditureType { get; set; }
		string ExpenditurePaymentType { get; set; }
	}
}