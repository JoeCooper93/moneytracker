﻿namespace MoneyTracker.Model
{
	public interface IIncomeModel
	{
		int IncomeAmount { get; set; }
		string IncomeType { get; set; }
	}
}