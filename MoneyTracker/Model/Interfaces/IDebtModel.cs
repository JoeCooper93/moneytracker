﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public interface IDebtModel
	{
		string DebtName { get; set; }
		int DebtIntrest { get; set; }
		int DebtAmount { get; set; }
	}
}
