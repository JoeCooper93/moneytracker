﻿namespace MoneyTracker.Model
{
	public interface IGoalsModel
	{
		int GoalAmount { get; set; }
		int GoalAmountAdded { get; set; }
		string GoalAmountAim { get; set; }
		int GoalAmountMinused { get; set; }
		string GoalName { get; set; }
	}
}