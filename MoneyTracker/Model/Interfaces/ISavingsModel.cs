﻿namespace MoneyTracker.Model
{
	public interface ISavingsModel
	{
		int SavingsAmount { get; set; }
		string SavingsName { get; set; }
	}
}