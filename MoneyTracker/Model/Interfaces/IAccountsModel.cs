﻿namespace MoneyTracker.Model
{
	public interface IAccountsModel
	{
		int AccountAmount { get; set; }
		string AccountName { get; set; }
		string AccountType { get; set; }

		void PrintStuff();
	}
}