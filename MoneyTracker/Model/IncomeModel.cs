﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public class IncomeModel : IIncomeModel
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }
		public string IncomeType { get; set; }
		public int IncomeAmount { get; set; }

	}
}
