﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTracker.Model
{
	public class GoalsModel : IGoalsModel
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }
		public string GoalName { get; set; }
		public string GoalAmountAim { get; set; }
		public int GoalAmount { get; set; }
		public int GoalAmountAdded { get; set; }
		public int GoalAmountMinused { get; set; }
	}
}
