﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneyTrackerClassLibrary;
using System.Reflection;
using MoneyTracker.Model;
using MoneyTracker.Services;
using MoneyTracker.DAL;

namespace MoneyTracker
{
	public static class ContainerConfig
	{
		public static IContainer Configure()
		{
			var builder = new ContainerBuilder();
			builder.RegisterType<Application>().As<IApplication>();
			builder.RegisterType<AccountsModel>().As<IAccountsModel>();
			builder.RegisterType<DebtModel>().As<IDebtModel>();
			builder.RegisterType<ExpenditureModel>().As<IExpenditureModel>();
			builder.RegisterType<GoalsModel>().As<IGoalsModel>();
			builder.RegisterType<IncomeModel>().As<IIncomeModel>();
			builder.RegisterType<SavingsModel>().As<ISavingsModel>();
			builder.RegisterType<ExpenditureService>().As<IExpenditureService>();
			

			//builder.RegisterAssemblyTypes(Assembly.Load(nameof(MoneyTrackerClassLibrary)))
			//	.Where(t => t.Namespace.Contains("Model"))
			//	.As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "i" + i.Name));

			return builder.Build();
		}
	}
}
