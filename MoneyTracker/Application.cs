﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneyTracker.Model;
using MoneyTracker.Services;

namespace MoneyTracker
{
	public class Application : IApplication
	{
		IAccountsModel _accountsModel;
		IDebtModel _debtModel;
		IExpenditureModel _expenditureModel;
		IGoalsModel _goalsModel;
		IIncomeModel _incomeModel;
		ISavingsModel _savingsModel;
		IExpenditureService _expenditureService;

		public Application(IAccountsModel accountsModel,
			IDebtModel debtModel,
			IExpenditureModel expenditureModel, 
			IGoalsModel goalsModel, 
			IIncomeModel incomeModel,
			ISavingsModel savingsModel,
			IExpenditureService expenditureService)
		{
			_accountsModel = accountsModel;
			_debtModel = debtModel;
			_expenditureModel = expenditureModel;
			_goalsModel = goalsModel;
			_incomeModel = incomeModel;
			_savingsModel = savingsModel;
			_expenditureService = expenditureService;
		}
		public void Run()
		{
			

		}

	}
}
