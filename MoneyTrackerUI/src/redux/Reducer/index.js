import MoneyReducer from './MoneyReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  money : MoneyReducer,
})


export default rootReducer;