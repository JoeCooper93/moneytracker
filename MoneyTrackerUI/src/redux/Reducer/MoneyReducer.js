import { Debt, Income, Expenditure, Saving } from '../constants/constants'

const initialState = {
    debt: [],
    income: [],
    expenditure: [],
    saving: []
}
function RecipePageReducer(state = initialState, action) {
    switch (action.type) {
        case Debt:
            return {
                ...state,

                debt: action.payload,
                loading: true
            }
        case Income:
            return {
                ...state,

                income: action.payload,
                loading: true
            }
        case Expenditure:
            return {
                ...state,

                expenditure: action.payload,
                loading: true
            }
            case Saving:
                return {
                    ...state,
    
                    saving: action.payload,
                    loading: true
                }
        default:
            return state
    }
};


export default RecipePageReducer;