import {Income, Expenditure, Saving, Debt } from "../constants/constants"

function addExpenditure(payload) {
    return {
        type: Expenditure,
        payload
    }
}
function setExpenditure() {
    return (dispatch => {
        dispatch({ type: Expenditure, 
            money })
    })
}

function addIncome(payload) {
    return {
        type: Income,
        payload
    }
}
function setIncome() {
    return (dispatch => {
        dispatch({ type: Income, 
            money })
    })
}
function addSaving(payload) {
    return {
        type: Saving,
        payload
    }
}
function setSaving() {
    return (dispatch => {
        dispatch({
            type: Saving,
            money
        })
    })
}
function addDebt(payload) {
    return {
        type: Debt,
        payload
    }
}
function setDebt() {
    return (dispatch => {
        dispatch({
            type: Debt,
            money
        })
    })
}

export {
    addDebt,
    addExpenditure,
    addIncome,
    addSaving,
    setDebt,
    setIncome,
    setSaving,
    setExpenditure
}