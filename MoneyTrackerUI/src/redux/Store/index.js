import { createStore } from 'redux';
import rootReducer from '../Reducer';
const preloadedState = window.__PRELOADED_STATE__

delete window.__PRELOADED_STATE__
let store = createStore(rootReducer, preloadedState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;