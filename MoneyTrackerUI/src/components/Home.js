import React, { useState } from 'react';
import { Col, Container, Row } from 'reactstrap';
import PieChart  from "./PieChart";
import PieData from '../PieData';
import ExpenditureTable from './Table/ExpenditureTable';
import IncomeTable from './Table/IncomeTable';
import SavingTable from './Table/SavingTable';
import DebtTable from './Table/DebtTable';
import './PieChart.css';
  
function  Home() {

    return (
    <Container >
      
        
      <ExpenditureTable/>
      <IncomeTable />
      <SavingTable/>
      <DebtTable/>
      <PieChart data={PieData}/>

    </Container>
    );
  }
export default Home;
