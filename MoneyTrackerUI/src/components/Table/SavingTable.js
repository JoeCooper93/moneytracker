import React, { useState } from 'react';
import { Table, Button } from 'reactstrap';

import { SAVING_API_URL } from '../../constants';

function SavingTable() {

    
  const [state, setState] = useState({});

 
  const getSaving = () => {
    fetch(SAVING_API_URL)
      .then(res => res.json())
      .then(res => setState({ saving: res }))
      .catch(err => console.log(err));
  }

  getSaving();

  const deleteItem = id => {
    let confirmDeletion = window.confirm('Do you really wish to delete it?');
    if (confirmDeletion) {
      fetch(`${SAVING_API_URL}/${id}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => {
          this.props.deleteItemFromState(id);
        })
        .catch(err => console.log(err));
    }
  }

    return( <Table striped>
      <thead className="thead-dark">
        <tr>
          <th>Name</th>
          <th>Amount</th>
        </tr>
      </thead>
      <tbody>
        {!state.saving || state.saving.length <= 0 ?
          <tr>
            <td colSpan="6" align="center"><b>No Users yet</b></td>
          </tr>
          : state.saving.map(item => (
            <tr key={item.id}>
              <td>
                {item.savingsName}
              </td>
              <td>
                {item.savingsAmount}
              </td>
              <td align="center">
                
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
    );
  }


export default SavingTable;
