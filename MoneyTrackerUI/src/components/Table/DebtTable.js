import React, { useState } from 'react';
import { Table, Button } from 'reactstrap';

import { DEBT_API_URL } from '../../constants';

function DebtTable() {

    
  const [state, setState] = useState({});

 
  const getDebt = () => {
    fetch(DEBT_API_URL)
      .then(res => res.json())
      .then(res => setState({ debt: res }))
      .catch(err => console.log(err));
  }

  getDebt();

  const deleteItem = id => {
    let confirmDeletion = window.confirm('Do you really wish to delete it?');
    if (confirmDeletion) {
      fetch(`${DEBT_API_URL}/${id}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => {
          this.props.deleteItemFromState(id);
        })
        .catch(err => console.log(err));
    }
  }

    return( <Table striped>
      <thead className="thead-dark">
        <tr>
          <th>Name</th>
          <th>Amount</th>
        </tr>
      </thead>
      <tbody>
        {!state.debt || state.debt.length <= 0 ?
          <tr>
            <td colSpan="6" align="center"><b>No Users yet</b></td>
          </tr>
          : state.debt.map(item => (
            <tr key={item.id}>
              <td>
                {item.debtName}
              </td>
              <td>
                {item.debtAmount}
              </td>
              <td align="center">
                
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
    );
  }


export default DebtTable;
