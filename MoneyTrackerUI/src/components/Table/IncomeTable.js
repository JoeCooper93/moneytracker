import React, { useState } from 'react';
import { Table, Button } from 'reactstrap';

import { INCOME_API_URL } from '../../constants';

function IncomeTable() {

    
  const [state, setState] = useState({});

 
  const getIncomes = () => {
    fetch(INCOME_API_URL)
      .then(res => res.json())
      .then(res => setState({ income: res }))
      .catch(err => console.log(err));
  }

  getIncomes();

  const deleteItem = id => {
    let confirmDeletion = window.confirm('Do you really wish to delete it?');
    if (confirmDeletion) {
      fetch(`${INCOME_API_URL}/${id}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => {
          this.props.deleteItemFromState(id);
        })
        .catch(err => console.log(err));
    }
  }

    return( <Table striped>
      <thead className="thead-dark">
        <tr>
          <th>Name</th>
          <th>Amount</th>
        </tr>
      </thead>
      <tbody>
        {!state.income || state.income.length <= 0 ?
          <tr>
            <td colSpan="6" align="center"><b>No Users yet</b></td>
          </tr>
          : state.income.map(item => (
            <tr key={item.id}>
              <td>
                {item.incomeType}
              </td>
              <td>
                {item.incomeAmount}
              </td>
              <td align="center">
                
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
    );
  }


export default IncomeTable;
