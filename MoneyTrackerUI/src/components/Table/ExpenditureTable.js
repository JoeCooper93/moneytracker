import React, { useState } from 'react';
import { Table, Button } from 'reactstrap';
import { EXPENDITURE_API_URL } from '../../constants';

function ExpenditureTable() {

  const [state, setState] = useState({});
  
  const deleteItem = id => {
    let confirmDeletion = window.confirm('Do you really wish to delete it?');
    if (confirmDeletion) {
      fetch(`${EXPENDITURE_API_URL}/${id}`, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => {
          this.props.deleteItemFromState(id);
        })
        .catch(err => console.log(err));
    }
  }
  const getExpenditures = () => {
    fetch(EXPENDITURE_API_URL)
      .then(res => res.json())
      .then(res => setState({ expenditure: res }))
      .catch(err => console.log(err));
  }
getExpenditures();
    return (
    <Table striped>
      <thead className="thead-dark">
        <tr>
          <th>Name</th>
          <th>Amount</th>
          <th>Expenditure Type</th>
        </tr>
      </thead>
      <tbody>
        {!state.expenditure || state.expenditure.length <= 0 ?
          <tr>
            <td colSpan="6" align="center"><b>No Users yet</b></td>
          </tr>
          : state.expenditure.map(item => (
            <tr key={item.id}>
              <td>
                {item.expenditureName}
              </td>
              <td>
                {item.expenditureAmount}
              </td>
              <td>
                {item.expenditurePaymentType}
              </td>
              <td align="center">
                
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
    );
}

export default ExpenditureTable;
