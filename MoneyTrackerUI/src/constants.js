export const EXPENDITURE_API_URL = 'https://localhost:44370/api/expenditure';
export const INCOME_API_URL = 'https://localhost:44370/api/income';
export const DEBT_API_URL = 'https://localhost:44370/api/debt';
export const SAVING_API_URL = 'https://localhost:44370/api/saving';