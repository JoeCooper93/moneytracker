const pieData = [
    {
      "id": "income",
      "label": "Income",
      "value": 2014.52,
      "color": "hsl(291, 70%, 50%)"
    },
    {
      "id": "savings",
      "label": "Savings",
      "value": 15500.00,
      "color": "hsl(6, 70%, 50%)"
    },
    {
      "id": "expenditure",
      "label": "Expenditure",
      "value": 750.00,
      "color": "hsl(224, 70%, 50%)"
    },
    
  ]
  export default pieData;